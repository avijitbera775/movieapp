const String url = "https://api.themoviedb.org/3";

String buildImageUrl(String imageId) {
  return "https://image.tmdb.org/t/p/w500/${imageId}";
}

String buildOriginalImageUrl(String imageId) {
  return "https://image.tmdb.org/t/p/original/${imageId}";
}

String buildAvatar(String path) {
  return "https://secure.gravatar.com/avatar/${path}";
}
