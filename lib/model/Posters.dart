// To parse this JSON data, do
//
//     final posters = postersFromJson(jsonString);

import 'dart:convert';

List<Posters> postersFromJson(dynamic str) =>
    List<Posters>.from(str.map((x) => Posters.fromJson(x)));

String postersToJson(List<Posters> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Posters {
  Posters({
    this.aspectRatio,
    this.height,
    this.iso6391,
    this.filePath,
    this.voteAverage,
    this.voteCount,
    this.width,
  });

  double? aspectRatio;
  int? height;
  String? iso6391;
  String? filePath;
  double? voteAverage;
  int? voteCount;
  int? width;

  factory Posters.fromJson(Map<String, dynamic> json) => Posters(
        aspectRatio: json["aspect_ratio"].toDouble(),
        height: json["height"],
        iso6391: json["iso_639_1"],
        filePath: json["file_path"],
        voteAverage: json["vote_average"].toDouble(),
        voteCount: json["vote_count"],
        width: json["width"],
      );

  Map<String, dynamic> toJson() => {
        "aspect_ratio": aspectRatio,
        "height": height,
        "iso_639_1": iso6391,
        "file_path": filePath,
        "vote_average": voteAverage,
        "vote_count": voteCount,
        "width": width,
      };
}
