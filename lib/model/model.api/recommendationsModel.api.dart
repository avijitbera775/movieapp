import 'package:movieapp/config/types.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Movie.dart';

class RecommendationModelApi {
  int? totalPage;
  int? currentPage;
  Success? success;
  Failer? failer;
  List<Movie>? recommendationList;
  ApiStatus apiStatus = ApiStatus.UNKNOWN;
  RecommendationModelApi(
      {this.failer,
      required this.apiStatus,
      this.recommendationList,
      this.success,
      this.currentPage,
      this.totalPage});

  List<Movie> get recommendations =>
      apiStatus == ApiStatus.FAILED ? [] : recommendationList!;
}
