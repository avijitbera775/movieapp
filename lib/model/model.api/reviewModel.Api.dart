import 'package:movieapp/config/types.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Reviews.dart';

class ReviewModelApi {
  ApiStatus apiStatus = ApiStatus.UNKNOWN;
  Success? success;
  Failer? failer;
  List<Reviews>? reviews;
  int? currentPage;
  int? totalPage;

  ReviewModelApi(
      {required this.apiStatus,
      this.currentPage,
      this.totalPage,
      this.failer,
      this.reviews,
      this.success});

  List<Reviews>? get reviewList => apiStatus == ApiStatus.FAILED ? [] : reviews;
}
