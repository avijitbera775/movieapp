import 'package:movieapp/config/types.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';

class SocialApiModel {
  ApiStatus apiStatus;
  Success? success;
  Failer? failer;

  String? facebookId;
  String? instagramId;
  String? twitterId;
  String? imdbId;
  String? id;

  SocialApiModel(
      {required this.apiStatus,
      this.facebookId,
      this.failer,
      this.id,
      this.imdbId,
      this.instagramId,
      this.success,
      this.twitterId});
}
