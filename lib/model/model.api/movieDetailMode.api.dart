import 'package:movieapp/config/types.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/MovieDetailModel.dart';

class MovieDetailApiModel {
  ApiStatus apiStatus = ApiStatus.UNKNOWN;
  Success? success;
  Failer? failer;
  MovieDetailModel? movieDetails;

  MovieDetailApiModel(
      {required this.apiStatus, this.failer, this.movieDetails, this.success});
}
