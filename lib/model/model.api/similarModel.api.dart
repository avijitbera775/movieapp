import 'package:movieapp/config/types.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Movie.dart';
import 'package:movieapp/model/Similar.dart';
import 'package:movieapp/widget/SimilarMovies.dart';

class SimilarModelApi {
  Success? success;
  Failer? failer;
  List<Movie>? similar;
  int? currentPage;
  int? totalPage;
  ApiStatus apiStatus = ApiStatus.UNKNOWN;

  SimilarModelApi(
      {this.failer,
      this.similar,
      this.success,
      required this.apiStatus,
      this.currentPage,
      this.totalPage});

  List<Movie> get similarMovies =>
      apiStatus == ApiStatus.SUCCESS ? similar! : [];
}
