import 'package:movieapp/config/types.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Movie.dart';

class PopulerModelApi {
  Success? success;
  Failer? failer;
  List<Movie>? movieList;
  ApiStatus apiStatus = ApiStatus.UNKNOWN;

  PopulerModelApi(
      {this.failer, this.movieList, this.success, required this.apiStatus});

  List<Movie> get movies => apiStatus == ApiStatus.SUCCESS ? movieList! : [];
}
