import 'package:movieapp/config/types.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Cast.dart';

class CastApiModel {
  Success? success;
  Failer? failer;
  List<Cast>? castList;
  ApiStatus apiStatus = ApiStatus.UNKNOWN;

  CastApiModel(
      {this.castList, this.failer, this.success, required this.apiStatus});

  List<Cast> get casts => apiStatus == ApiStatus.FAILED ? [] : castList!;
}
