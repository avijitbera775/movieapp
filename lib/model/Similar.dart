// To parse this JSON data, do
//
//     final similar = similarFromJson(jsonString);

import 'dart:convert';

import 'package:movieapp/model/Movie.dart';

Similar similarFromJson(String str) => Similar.fromJson(json.decode(str));

String similarToJson(Similar data) => json.encode(data.toJson());

class Similar {
  Similar({
    required this.page,
    this.results,
    required this.totalPages,
    required this.totalResults,
  });

  int page;
  List<Movie>? results;
  int totalPages;
  int totalResults;

  factory Similar.fromJson(Map<String, dynamic> json) => Similar(
        page: json["page"],
        results: json["results"] == null
            ? null
            : List<Movie>.from(json["results"].map((x) => Movie.fromJson(x))),
        totalPages: json["total_pages"],
        totalResults: json["total_results"],
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "results": results == null
            ? null
            : List<dynamic>.from(results!.map((x) => x.toJson())),
        "total_pages": totalPages,
        "total_results": totalResults,
      };
}
