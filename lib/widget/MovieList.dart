import 'package:flutter/material.dart';
import 'package:movieapp/model/Movie.dart';

import 'MovieItem.dart';

class MovieList extends StatelessWidget {
  final List<Movie> movies;
  final String title;
  const MovieList({Key? key, required this.movies, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 10,
        ),
        Text(
          title,
          style: TextStyle(
              fontSize: 18,
              color: Colors.grey[100],
              fontWeight: FontWeight.w600),
        ),
        SizedBox(
          height: 200,
          child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: movies.length,
              physics: const AlwaysScrollableScrollPhysics(),
              itemBuilder: (ctx, index) {
                return MovieItem(
                  movie: movies[index],
                  isFirst: index == 0,
                  isLast: movies.length == index,
                );
              }),
        ),
      ],
    );
  }
}
