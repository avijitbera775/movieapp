import 'package:flutter/material.dart';
import 'package:movieapp/cubit/moviedetails_cubit.dart';
import 'package:movieapp/cubit/recommendationmovie_cubit.dart';
import 'package:movieapp/widget/MovieItem.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RecommendationBar extends StatelessWidget {
  const RecommendationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecommendationmovieCubit, RecommendationmovieState>(
      builder: (ctx, state) {
        if (state is RecommendationmovieLoaded) {
          return SizedBox(
            height: 200,
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: state.recommendList!.length,
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (ctx, index) {
                  return MovieItem(
                    movie: state.recommendList![index],
                    isFirst: index == 0,
                    isLast: state.recommendList!.length == index,
                  );
                }),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
