import 'package:flutter/material.dart';
import 'package:movieapp/config/url.dart';
import 'package:movieapp/model/Movie.dart';
import 'package:movieapp/screen/MovieDetails.dart';

class MovieItem extends StatelessWidget {
  final Movie movie;
  final bool isFirst;
  final bool isLast;

  const MovieItem(
      {Key? key,
      required this.movie,
      required this.isFirst,
      required this.isLast})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 125,
      height: 180,
      margin: EdgeInsets.only(
          right: isLast ? 0 : 7, left: isFirst ? 0 : 7, top: 10, bottom: 5),
      decoration: const BoxDecoration(color: Colors.transparent),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Image(
                fit: BoxFit.fill,
                image: NetworkImage(buildImageUrl(movie.posterPath))),
          ),
          LayoutBuilder(builder: (ctx, boxConstraints) {
            return Material(
                color: Colors.transparent,
                child: InkWell(
                    borderRadius: BorderRadius.circular(12),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => MovieDetails(
                                id: movie.id,
                              )));
                    },
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: 70,
                        padding: EdgeInsets.only(left: 6, right: 5),
                        width: boxConstraints.maxWidth,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(12),
                                bottomRight: Radius.circular(12)),
                            color: Color.fromRGBO(255, 255, 255, 0.5)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              movie.title,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.grey[900],
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.star,
                                  color: Colors.grey[900],
                                  size: 15,
                                ),
                                const SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  "${movie.voteAverage}/10",
                                  style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    )));
          }),
        ],
      ),
    );
  }
}
