import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movieapp/cubit/toprated_cubit.dart';

import 'MovieItem.dart';

class TopRatedMovies extends StatelessWidget {
  const TopRatedMovies({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TopratedCubit, TopratedState>(
      builder: (ctx, movieState) {
        if (movieState is TopratedLoaded) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 10,
              ),
              Text(
                "Top Rated",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.grey[100],
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 200,
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: movieState.popularList!.length,
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemBuilder: (ctx, index) {
                      return MovieItem(
                        movie: movieState.popularList![index],
                        isFirst: index == 0,
                        isLast: movieState.popularList!.length == index,
                      );
                    }),
              ),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }
}
