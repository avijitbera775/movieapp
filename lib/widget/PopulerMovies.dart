import 'package:flutter/material.dart';
import 'package:movieapp/cubit/popularmovie_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movieapp/widget/MovieItem.dart';
import 'package:movieapp/widget/MovieList.dart';

class PopulerMovies extends StatelessWidget {
  const PopulerMovies({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PopularmovieCubit, PopularmovieState>(
      builder: (ctx, movieState) {
        if (movieState is PopularmovieLoaded) {
          return MovieList(
              movies: movieState.popularList!, title: "Populer Movies");
        } else {
          return Container();
        }
      },
    );
  }
}
