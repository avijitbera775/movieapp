import 'package:flutter/material.dart';

class CategoryBar extends StatefulWidget {
  @override
  State<CategoryBar> createState() => _CategoryBarState();
}

class _CategoryBarState extends State<CategoryBar> {
  int? selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: ListView(
        shrinkWrap: false,
        scrollDirection: Axis.horizontal,
        children: [
          selectedButton(
              name: "All",
              index: 1,
              isSelected: selectedIndex == 1,
              onTab: (int index) {
                setState(() {
                  selectedIndex = index;
                });
              }),
          selectedButton(
              name: "Movies",
              isSelected: selectedIndex == 2,
              index: 2,
              onTab: (int index) {
                setState(() {
                  selectedIndex = index;
                });
              }),
          selectedButton(
              name: "Web Serice",
              index: 3,
              isSelected: selectedIndex == 3,
              onTab: (int index) {
                setState(() {
                  selectedIndex = index;
                });
              }),
        ],
      ),
    );
  }
}

Widget selectedButton(
    {required String name,
    required Function(int) onTab,
    required int index,
    required bool isSelected}) {
  return Container(
    margin: EdgeInsets.only(
      right: 12,
      left: 4,
    ),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        gradient: isSelected
            ? LinearGradient(colors: [Colors.purple[500]!, Colors.purple[700]!])
            : LinearGradient(colors: [Colors.grey[300]!, Colors.grey[200]!])),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        borderRadius: BorderRadius.circular(8),
        onTap: () => onTab(index),
        child: Padding(
          padding:
              const EdgeInsets.only(top: 5, bottom: 5, left: 12, right: 12),
          child: Center(
            child: Text(
              name,
              style: TextStyle(
                  color: isSelected ? Colors.grey[200] : Colors.grey[800],
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0.8),
            ),
          ),
        ),
      ),
    ),
  );
}
