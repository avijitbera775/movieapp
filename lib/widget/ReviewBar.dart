import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movieapp/config/url.dart';
import 'package:movieapp/cubit/review_cubit.dart';

class ReviewBar extends StatelessWidget {
  const ReviewBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewCubit, ReviewState>(
      builder: (context, reviewState) {
        if (reviewState is ReviewLoaded) {
          return Container(
            height: 80,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: reviewState.reviewList!.length,
                shrinkWrap: true,
                itemBuilder: (ctx, index) {
                  return Container(
                    height: 100,
                    width: MediaQuery.of(context).size.width / 1.5,
                    margin: EdgeInsets.only(left: 8, right: 8),
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: Colors.grey[900],
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: Image(
                                  height: 40,
                                  width: 40,
                                  image: NetworkImage(buildAvatar(reviewState
                                      .reviewList![index]
                                      .authorDetails!
                                      .avatarPath!))),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  reviewState.reviewList![index].author!,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: Colors.grey[200],
                                      fontSize: 16,
                                      letterSpacing: 0.8,
                                      fontWeight: FontWeight.w400),
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.star,
                                      color: Colors.grey[300],
                                      size: 14,
                                    ),
                                    const SizedBox(
                                      width: 6,
                                    ),
                                    Text(
                                      "${reviewState.reviewList![index].authorDetails!.rating ?? 0}/10",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1,
                                          color: Colors.grey[300]),
                                    )
                                  ],
                                ),
                                // Expanded(
                                //     child: Text(
                                //   reviewState.reviewList![index].content!,
                                //   maxLines: 1,
                                //   overflow: TextOverflow.ellipsis,
                                // )),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                }),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
