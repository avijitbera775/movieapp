import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movieapp/cubit/similarmovie_cubit.dart';
import 'package:movieapp/widget/MovieItem.dart';

class SimilarBar extends StatelessWidget {
  const SimilarBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SimilarmovieCubit, SimilarmovieState>(
      builder: (ctx, similarState) {
        if (similarState is SimilarmovieLoaded) {
          return SizedBox(
            height: 200,
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: similarState.similarMovies!.length,
                physics: AlwaysScrollableScrollPhysics(),
                itemBuilder: (ctx, index) {
                  return MovieItem(
                    movie: similarState.similarMovies![index],
                    isFirst: index == 0,
                    isLast: similarState.similarMovies!.length == index,
                  );
                }),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
