import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movieapp/cubit/upcoming_cubit.dart';
import 'package:movieapp/widget/MovieList.dart';

import 'MovieItem.dart';

class UpcomingMovie extends StatelessWidget {
  const UpcomingMovie({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpcomingCubit, UpcomingState>(
      builder: (ctx, movieState) {
        if (movieState is UpcomingLoaded) {
          return MovieList(
              movies: movieState.upcomingList!, title: "Upcoming Movies");
        } else {
          return Container();
        }
      },
    );
  }
}
