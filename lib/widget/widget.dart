export 'CategoryBar.dart';
export 'MovieItem.dart';
export 'PopulerMovies.dart';
export 'RecommendationBar.dart';
export 'SearchBar.dart';
export 'SimilarBar.dart';
export 'SimilarMovies.dart';
