import 'package:flutter/material.dart';
import 'package:movieapp/config/action.dart';
import 'package:movieapp/config/colors.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/config/url.dart';
import 'package:movieapp/cubit/cast_cubit.dart';
import 'package:movieapp/cubit/moviedetails_cubit.dart';
import 'package:movieapp/cubit/recommendationmovie_cubit.dart';
import 'package:movieapp/cubit/review_cubit.dart';
import 'package:movieapp/cubit/similarmovie_cubit.dart';

import 'package:movieapp/model/MovieDetailModel.dart';
import 'package:movieapp/screen/Home.dart';
import 'package:movieapp/widget/MovieItem.dart';
import 'package:movieapp/widget/RecommendationBar.dart';
import 'package:movieapp/widget/ReviewBar.dart';
import 'package:movieapp/widget/SimilarBar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MovieDetails extends StatefulWidget {
  final int id;
  const MovieDetails({Key? key, required this.id}) : super(key: key);

  @override
  State<MovieDetails> createState() => _MovieDetailsState();
}

class _MovieDetailsState extends State<MovieDetails> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // context.read<MovieProvider>().getMovieDetails(movieId: widget.id);
    // context.read<CastProvider>().getMovieCast(movieId: widget.id);
    // context.read<MovieProvider>().getRecommendationList(movieId: widget.id);
    // context.read<MovieProvider>().getSimilarList(movieId: widget.id);
    BlocProvider.of<MoviedetailsCubit>(context)
        .getMovieDetails(movieId: widget.id);
    BlocProvider.of<SimilarmovieCubit>(context).getSimilar(movieId: widget.id);
    BlocProvider.of<RecommendationmovieCubit>(context)
        .getRecommendation(movieId: widget.id);
    BlocProvider.of<CastCubit>(context).getCast(movieId: widget.id);

    BlocProvider.of<ReviewCubit>(context).getReviews(movieId: widget.id);
  }

  @override
  void dispose() {
    BlocProvider.of<MoviedetailsCubit>(context).dispatch();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (ctx) => const Home()));
        return Future.value(true);
      },
      child: BlocBuilder<MoviedetailsCubit, MoviedetailsState>(
          builder: (ctx, state) {
        if (state is MoviedetailsLoaded) {
          return Scaffold(
              backgroundColor: bgColor,
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height / 1.5,
                      width: MediaQuery.of(context).size.width,
                      child: Stack(
                        children: [
                          Image(
                            height: MediaQuery.of(context).size.height,
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.cover,
                            image: NetworkImage(buildOriginalImageUrl(
                                state.movieDetail!.backdropPath!)),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: LayoutBuilder(builder: (context, ct) {
                              return Container(
                                width: ct.maxWidth,
                                height: ct.maxHeight / 4.3,
                                decoration: const BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        colors: [
                                      // ignore: unnecessary_const
                                      Color.fromRGBO(0, 0, 0, 0.2),
                                      Color.fromRGBO(0, 0, 0, 0.8),
                                    ])),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10, bottom: 10),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        state.movieDetail!.originalTitle!,
                                        style: TextStyle(
                                            color: Colors.grey[100],
                                            fontSize: 17,
                                            fontWeight: FontWeight.w700),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      createGenres(state.movieDetail!.genres),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      IntrinsicHeight(
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.star,
                                              color: Colors.grey[300],
                                              size: 14,
                                            ),
                                            const SizedBox(
                                              width: 6,
                                            ),
                                            Text(
                                              "${state.movieDetail!.voteAverage}/10",
                                              style: TextStyle(
                                                  color: Colors.grey[300],
                                                  fontWeight: FontWeight.w400,
                                                  letterSpacing: 0.6,
                                                  fontSize: 12),
                                            ),
                                            VerticalDivider(
                                              color: Colors.grey[100],
                                              thickness: 1,
                                            ),
                                            Expanded(
                                              child: createLanguages(state
                                                  .movieDetail!
                                                  .spokenLanguages!),
                                            ),
                                            VerticalDivider(
                                              color: Colors.grey[100],
                                              thickness: 1,
                                            ),
                                            Text(
                                              durationToString(
                                                  state.movieDetail!.runtime!),
                                              style: TextStyle(
                                                  color: Colors.grey[300],
                                                  fontWeight: FontWeight.w400,
                                                  letterSpacing: 0.6,
                                                  fontSize: 12),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin:
                          const EdgeInsets.only(top: 10, left: 15, right: 15),
                      child: Text(
                        state.movieDetail!.overview!,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.grey[100],
                            fontWeight: FontWeight.w400,
                            letterSpacing: 0.8),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 15, top: 15),
                      child: Text(
                        "Production Companies",
                        style: TextStyle(
                            color: Colors.grey[100],
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1),
                      ),
                    ),
                    Container(
                        margin:
                            const EdgeInsets.only(top: 10, left: 15, right: 15),
                        child: productionList(
                            state.movieDetail!.productionCompanies!)),
                    Container(
                      margin:
                          const EdgeInsets.only(top: 10, left: 15, right: 15),
                      child: Text(
                        "Budget:- ${numberFormater(
                          state.movieDetail!.budget!,
                        )}",
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey[100]),
                      ),
                    ),
                    Container(
                      margin:
                          const EdgeInsets.only(top: 5, left: 15, right: 15),
                      child: Text(
                          "Revenue:- ${numberFormater(state.movieDetail!.revenue!)}",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey[100])),
                    ),
                    // CastBar()
                    BlocBuilder<CastCubit, CastState>(
                      builder: (ctx, state) {
                        if (state is CastLoaded) {
                          return Container(
                            margin: const EdgeInsets.only(
                                top: 5, left: 15, right: 15),
                            child: Text(
                              "Casts",
                              style: TextStyle(
                                  color: Colors.grey[100]!,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1),
                            ),
                          );
                        } else {
                          return const SizedBox();
                        }
                      },
                    ),
                    BlocBuilder<CastCubit, CastState>(
                      builder: (ctx, state) {
                        if (state is CastLoaded) {
                          return Container(
                            margin: const EdgeInsets.only(
                                left: 15, right: 15, top: 10),
                            height: 120,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: state.castList!.length > 6
                                    ? 7
                                    : state.castList!.length,
                                shrinkWrap: true,
                                itemBuilder: (ctx, index) {
                                  return Container(
                                    margin: EdgeInsets.only(
                                        right: index == state.castList!.length
                                            ? 0
                                            : 5),
                                    child: index == 6
                                        ? Material(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            color: Colors.grey[900],
                                            child: InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              onTap: () {},
                                              child: Container(
                                                height: 120,
                                                width: 75,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                    color: Colors.transparent),
                                                child: Center(
                                                  child: Icon(
                                                    Icons.arrow_forward,
                                                    color: Colors.grey[200],
                                                    size: 18,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                        : LayoutBuilder(builder: (context, ct) {
                                            return ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              child: Image(
                                                image: NetworkImage(
                                                    buildImageUrl(state
                                                        .castList![index]
                                                        .profilePath!)),
                                              ),
                                            );
                                          }),
                                  );
                                }),
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),

                    BlocBuilder<SimilarmovieCubit, SimilarmovieState>(
                      builder: (ctx, state) {
                        if (state is SimilarmovieLoaded) {
                          return Container(
                            margin: const EdgeInsets.only(
                              left: 15,
                              top: 15,
                            ),
                            child: Text(
                              "Similar Movies",
                              style: TextStyle(
                                  color: Colors.grey[100],
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 5, top: 5, right: 5),
                      child: const SimilarBar(),
                    ),
                    BlocBuilder<RecommendationmovieCubit,
                        RecommendationmovieState>(
                      builder: (ctx, state) {
                        if (state is RecommendationmovieLoaded) {
                          return Container(
                            margin: const EdgeInsets.only(
                              left: 15,
                              top: 15,
                            ),
                            child: Text(
                              "Recommendations",
                              style: TextStyle(
                                  color: Colors.grey[100],
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),

                    Container(
                      margin: const EdgeInsets.only(left: 5, top: 5, right: 5),
                      child: const RecommendationBar(),
                    ),
                    BlocBuilder<ReviewCubit, ReviewState>(
                      builder: (ctx, state) {
                        if (state is ReviewLoaded) {
                          return Container(
                            margin: const EdgeInsets.only(
                              left: 15,
                              top: 15,
                            ),
                            child: Text(
                              "Reviews",
                              style: TextStyle(
                                  color: Colors.grey[100],
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                    // const ReviewBar()
                  ],
                ),
              ));
        } else {
          return Container();
        }
      }),
    );
  }

  Widget productionList(List<ProductionCompany> production) {
    return Container(
      height: 36,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: production.length,
          shrinkWrap: true,
          itemBuilder: (ctx, index) {
            return Container(
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                margin: EdgeInsets.only(
                    left: index == 0 ? 0 : 5,
                    right: index == production.length ? 0 : 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    gradient: LinearGradient(
                        colors: [Colors.purple[500]!, Colors.purple[700]!])),
                child: Center(
                    child: Text(
                  production[index].name!,
                  style: TextStyle(
                      color: Colors.grey[200], fontWeight: FontWeight.w500),
                )));
          }),
    );
  }

  Widget createLanguages(List<SpokenLanguage> language) {
    List<String> list = [];

    language.forEach((item) {
      list.add(item.englishName!);
    });

    return Text(
      list.join(' - '),
      textAlign: TextAlign.center,
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
      style: TextStyle(
        color: Colors.grey[300],
        fontWeight: FontWeight.w400,
        letterSpacing: 0.6,
        fontSize: 12,
      ),
    );
  }

  String durationToString(int minutes) {
    var d = Duration(minutes: minutes);
    List<String> parts = d.toString().split(':');
    return '${parts[0].padLeft(2, '0')}h ${parts[1].padLeft(2, '0')}min';
  }

  Widget createGenres(List<Genre>? genres) {
    List<String> list = [];

    genres!.forEach((item) {
      list.add(item.name!);
    });

    return Text(
      list.join(' - '),
      style: TextStyle(
          color: Colors.grey[300],
          fontWeight: FontWeight.w400,
          letterSpacing: 0.6,
          fontSize: 12),
    );
  }
}
