import 'package:flutter/material.dart';
import 'package:movieapp/config/colors.dart';
import 'package:movieapp/cubit/popularmovie_cubit.dart';
import 'package:movieapp/cubit/toprated_cubit.dart';
import 'package:movieapp/cubit/upcoming_cubit.dart';

import 'package:movieapp/repository/movieApi.dart';
import 'package:movieapp/widget/CategoryBar.dart';
import 'package:movieapp/widget/PopulerMovies.dart';
import 'package:movieapp/widget/SearchBar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movieapp/widget/TopRated.dart';
import 'package:movieapp/widget/UpcomingMovie.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // context.read<MovieProvider>().getPopulerMovies();
    // Provider.of<MovieProvider>(context).getPopulerMovies();
    BlocProvider.of<PopularmovieCubit>(context).getPopuler();
    BlocProvider.of<TopratedCubit>(context).getTopRated();
    BlocProvider.of<UpcomingCubit>(context).getUpcomingMovies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: bgColor,
        body: SafeArea(
          child: Column(
            children: [
              SearchBar(),
              SizedBox(
                height: 10,
              ),
              Expanded(
                flex: 1,
                child: Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    child: CategoryBar()),
              ),
              Expanded(
                  flex: 19,
                  child: Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    child: ListView(
                      children: const [
                        PopulerMovies(),
                        TopRatedMovies(),
                        UpcomingMovie()
                      ],
                    ),
                  )),
            ],
          ),
        ));
  }
}
