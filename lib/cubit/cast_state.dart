part of 'cast_cubit.dart';

@immutable
abstract class CastState {
  const CastState();
}

class CastInitial extends CastState {
  const CastInitial();
}

class CastError extends CastState {
  String message;
  CastError(this.message);
}

class CastLoading extends CastState {
  const CastLoading();
}

class CastLoaded extends CastState {
  List<Cast>? castList;

  set setCast(List<Cast> cast) => castList = cast;
}
