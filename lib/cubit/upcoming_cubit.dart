import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:movieapp/cubit/toprated_cubit.dart';
import 'package:movieapp/repository/movieApi.dart';

import '../config/types.dart';
import '../model/Movie.dart';
import '../model/model.api/populerModel.Api.dart';

part 'upcoming_state.dart';

class UpcomingCubit extends Cubit<UpcomingState> {
  MovieApi _api;
  UpcomingCubit(this._api) : super(UpcomingInitial());

  Future<void> getUpcomingMovies() async {
    PopulerModelApi model = await _api.getUpcomingMovieApi();
    if (model.apiStatus == ApiStatus.FAILED) {
      emit(UpcomingError(model.failer!.message));
    } else {
      var top = UpcomingLoaded();
      top.setPopular = model.movies;
      emit(top);
    }
  }
}
