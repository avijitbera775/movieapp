import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/model/Cast.dart';
import 'package:movieapp/model/model.api/castModel.Api.dart';
import 'package:movieapp/repository/castApi.dart';

part 'cast_state.dart';

class CastCubit extends Cubit<CastState> {
  CastApi _apt;
  CastCubit(this._apt) : super(CastInitial());

  Future<void> getCast({required int movieId}) async {
    CastApiModel _apimodel = await _apt.getCastApi(movieId: movieId);

    if (_apimodel.apiStatus == ApiStatus.FAILED) {
      emit(CastError(_apimodel.failer!.message));
    } else {
      var cast = CastLoaded();
      cast.setCast = _apimodel.castList!;
      emit(cast);
    }
  }
}
