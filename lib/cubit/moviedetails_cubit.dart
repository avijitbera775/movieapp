import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/model/MovieDetailModel.dart';
import 'package:movieapp/model/model.api/movieDetailMode.api.dart';
import 'package:movieapp/repository/movieApi.dart';

part 'moviedetails_state.dart';

class MoviedetailsCubit extends Cubit<MoviedetailsState> {
  final MovieApi _api;
  MoviedetailsCubit(this._api) : super(const MoviedetailsInitial());

  void dispatch() {
    emit(MoviedetailsLoaded(null));
  }

  Future<void> getMovieDetails({required int movieId}) async {
    MovieDetailApiModel? _apimodel =
        await _api.getMovieDetails(movieId: movieId);

    if (_apimodel!.apiStatus == ApiStatus.FAILED) {
      emit(MoviedetailsError(_apimodel.failer!.message));
    } else {
      emit(MoviedetailsLoaded(_apimodel.movieDetails!));
    }
  }
}
