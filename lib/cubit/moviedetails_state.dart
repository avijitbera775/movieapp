part of 'moviedetails_cubit.dart';

@immutable
abstract class MoviedetailsState {
  const MoviedetailsState();
}

class MoviedetailsInitial extends MoviedetailsState {
  const MoviedetailsInitial();
}

class MoviedetailsLoading extends MoviedetailsState {
  const MoviedetailsLoading();
}

class MoviedetailsError extends MoviedetailsState {
  String message;
  MoviedetailsError(this.message);
}

class MoviedetailsLoaded extends MoviedetailsState {
  final MovieDetailModel? movieDetail;
  const MoviedetailsLoaded(this.movieDetail);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MoviedetailsLoaded && other.movieDetail == movieDetail;
  }

  @override
  int get hashCode => movieDetail.hashCode;
}
