import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/model/Reviews.dart';
import 'package:movieapp/repository/reviewApi.dart';

part 'review_state.dart';

class ReviewCubit extends Cubit<ReviewState> {
  ReviewApi _api;

  ReviewCubit(this._api) : super(ReviewInitial());

  Future<void> getReviews({required int movieId}) async {
    var _model = await _api.getMovieReviewsApi(movieId: movieId);

    if (_model.apiStatus == ApiStatus.FAILED) {
      emit(ReviewError(_model.failer!.message));
    } else {
      var rev = ReviewLoaded();
      rev.setReview = _model.reviews!;
      rev.setTotalPage = _model.totalPage!;
      rev.setCurrentPage = _model.currentPage!;
      emit(rev);
    }
  }
}
