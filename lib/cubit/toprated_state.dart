part of 'toprated_cubit.dart';

@immutable
abstract class TopratedState {
  const TopratedState();
}

class TopratedError extends TopratedState {
  final String message;
  const TopratedError(this.message);
}

class TopratedLoading extends TopratedState {
  const TopratedLoading();
}

class TopratedLoaded extends TopratedState {
  List<Movie>? popularList;

  set setPopular(List<Movie> movie) => popularList = movie;
  // const PopularmovieLoaded();

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TopratedLoaded &&
        listEquals(other.popularList, popularList);
  }

  @override
  int get hashCode => popularList.hashCode;
}

class TopratedInitial extends TopratedState {
  const TopratedInitial();
}
