import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/repository/movieApi.dart';

import '../model/Movie.dart';
import '../model/model.api/populerModel.Api.dart';

part 'toprated_state.dart';

class TopratedCubit extends Cubit<TopratedState> {
  MovieApi _movieApi;

  TopratedCubit(this._movieApi) : super(TopratedInitial());

  Future<void> getTopRated() async {
    PopulerModelApi model = await _movieApi.getTopRatedMovies();
    if (model.apiStatus == ApiStatus.FAILED) {
      emit(TopratedError(model.failer!.message));
    } else {
      var top = TopratedLoaded();
      top.setPopular = model.movies;
      emit(top);
    }
  }
}
