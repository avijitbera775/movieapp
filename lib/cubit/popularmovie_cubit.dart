import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/model/Movie.dart';
import 'package:movieapp/model/model.api/populerModel.Api.dart';
import 'package:movieapp/repository/movieApi.dart';

part 'popularmovie_state.dart';

class PopularmovieCubit extends Cubit<PopularmovieState> {
  MovieApi _movieApi;
  PopularmovieCubit(this._movieApi) : super(PopularmovieInitial());

  Future<void> getPopuler() async {
    PopulerModelApi _api = await _movieApi.getPopulerMovies();
    if (_api.apiStatus == ApiStatus.FAILED) {
      emit(PopularmovieError(_api.failer!.message));
    } else {
      var pop = PopularmovieLoaded();
      pop.setPopular = _api.movies;
      emit(pop);
    }
  }
}
