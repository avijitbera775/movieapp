part of 'similarmovie_cubit.dart';

@immutable
abstract class SimilarmovieState {
  const SimilarmovieState();
}

class SimilarmovieInitial extends SimilarmovieState {
  const SimilarmovieInitial();
}

class SimilarmovieError extends SimilarmovieState {
  final String message;
  const SimilarmovieError(this.message);
}

class SimilarmovieLoading extends SimilarmovieState {
  const SimilarmovieLoading();
}

class SimilarmovieLoaded extends SimilarmovieState {
  List<Movie>? similarMovies;
  int? currentPage;
  int? totalPage;

  set setTotalPage(int page) => totalPage = page;
  set setCurrentPage(int page) => currentPage = page;

  set setSimilar(List<Movie> movie) => similarMovies = movie;

  // SimilarmovieState setSimilar() {}
}
