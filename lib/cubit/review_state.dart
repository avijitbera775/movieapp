part of 'review_cubit.dart';

@immutable
abstract class ReviewState {
  const ReviewState();
}

class ReviewInitial extends ReviewState {
  const ReviewInitial();
}

class ReviewError extends ReviewState {
  String message;
  ReviewError(this.message);
}

class ReviewLoading extends ReviewState {
  const ReviewLoading();
}

class ReviewLoaded extends ReviewState {
  List<Reviews>? reviewList;
  int? currentPage;
  int? totalPage;

  set setCurrentPage(int page) => currentPage = page;
  set setTotalPage(int page) => totalPage = page;
  set setReview(List<Reviews> review) => reviewList = review;
}
