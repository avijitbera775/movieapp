part of 'recommendationmovie_cubit.dart';

@immutable
abstract class RecommendationmovieState {
  const RecommendationmovieState();
}

class RecommendationmovieInitial extends RecommendationmovieState {
  const RecommendationmovieInitial();
}

class RecommendationmovieLoading extends RecommendationmovieState {
  const RecommendationmovieLoading();
}

class RecommendationmovieError extends RecommendationmovieState {
  String message;
  RecommendationmovieError(this.message);
}

class RecommendationmovieLoaded extends RecommendationmovieState {
  List<Movie>? recommendList;
  int? currentPage;
  int? totalPage;

  set setCurrentPage(int page) => currentPage = page;
  set setTotalPage(int page) => totalPage = page;
  set setRecommend(List<Movie> movie) => recommendList = movie;

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RecommendationmovieLoaded &&
        listEquals(other.recommendList, recommendList);
  }

  @override
  int get hashCode => recommendList.hashCode;
}
