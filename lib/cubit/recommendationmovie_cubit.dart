import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/model/Movie.dart';
import 'package:flutter/foundation.dart';
import 'package:movieapp/model/model.api/recommendationsModel.api.dart';
import 'package:movieapp/repository/recommendationsApi.dart';
part 'recommendationmovie_state.dart';

class RecommendationmovieCubit extends Cubit<RecommendationmovieState> {
  RecommendationsApi _api;
  RecommendationmovieCubit(this._api)
      : super(const RecommendationmovieInitial());

  Future<void> getRecommendation({required int movieId}) async {
    RecommendationModelApi _modelApi =
        await _api.getRecommendationsApi(movieId: movieId);
    if (_modelApi.apiStatus == ApiStatus.FAILED) {
      emit(RecommendationmovieError(_modelApi.failer!.message));
    } else {
      var rec = RecommendationmovieLoaded();
      rec.setRecommend = _modelApi.recommendations;

      rec.setCurrentPage = _modelApi.currentPage!;
      rec.setTotalPage = _modelApi.totalPage!;

      emit(rec);
    }
  }
}
