part of 'upcoming_cubit.dart';

@immutable
abstract class UpcomingState {
  const UpcomingState();
}

class UpcomingInitial extends UpcomingState {
  const UpcomingInitial();
}

class UpcomingError extends UpcomingState {
  final String message;
  const UpcomingError(this.message);
}

class UpcomingLoading extends UpcomingState {
  const UpcomingLoading();
}

class UpcomingLoaded extends UpcomingState {
  List<Movie>? upcomingList;

  set setPopular(List<Movie> movie) => upcomingList = movie;
  // const PopularmovieLoaded();

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UpcomingLoaded &&
        listEquals(other.upcomingList, upcomingList);
  }

  @override
  int get hashCode => upcomingList.hashCode;
}
