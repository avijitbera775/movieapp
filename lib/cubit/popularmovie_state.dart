part of 'popularmovie_cubit.dart';

@immutable
abstract class PopularmovieState {
  const PopularmovieState();
}

class PopularmovieInitial extends PopularmovieState {
  const PopularmovieInitial();
}

class PopularmovieError extends PopularmovieState {
  final String message;
  const PopularmovieError(this.message);
}

class PopularmovieLoading extends PopularmovieState {
  const PopularmovieLoading();
}

class PopularmovieLoaded extends PopularmovieState {
  List<Movie>? popularList;

  set setPopular(List<Movie> movie) => popularList = movie;
  // const PopularmovieLoaded();

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is PopularmovieLoaded &&
        listEquals(other.popularList, popularList);
  }

  @override
  int get hashCode => popularList.hashCode;
}
