import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/model/Movie.dart';
import 'package:movieapp/model/model.api/similarModel.api.dart';
import 'package:movieapp/repository/similarApi.dart';

part 'similarmovie_state.dart';

class SimilarmovieCubit extends Cubit<SimilarmovieState> {
  final SimilarApi _similarApi;
  SimilarmovieCubit(this._similarApi) : super(const SimilarmovieInitial());

  Future<void> getSimilar({required int movieId}) async {
    SimilarModelApi _api = await _similarApi.getSimilarApi(movieId: movieId);
    if (_api.apiStatus == ApiStatus.FAILED) {
      emit(SimilarmovieError(_api.failer!.message));
    } else {
      var sim = SimilarmovieLoaded();
      sim.setSimilar = _api.similarMovies;
      sim.setCurrentPage = _api.currentPage!;
      sim.setTotalPage = _api.totalPage!;
      emit(sim);
    }
  }
}
