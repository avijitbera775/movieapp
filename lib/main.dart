import 'package:flutter/material.dart';
import 'package:movieapp/cubit/cast_cubit.dart';
import 'package:movieapp/cubit/moviedetails_cubit.dart';
import 'package:movieapp/cubit/popularmovie_cubit.dart';
import 'package:movieapp/cubit/recommendationmovie_cubit.dart';
import 'package:movieapp/cubit/review_cubit.dart';
import 'package:movieapp/cubit/similarmovie_cubit.dart';
import 'package:movieapp/cubit/toprated_cubit.dart';
import 'package:movieapp/cubit/upcoming_cubit.dart';
import 'package:movieapp/repository/castApi.dart';
import 'package:movieapp/repository/movieApi.dart';
import 'package:movieapp/repository/recommendationsApi.dart';
import 'package:movieapp/repository/reviewApi.dart';
import 'package:movieapp/repository/similarApi.dart';
import 'package:movieapp/screen/screens.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<PopularmovieCubit>(
          create: (_) => PopularmovieCubit(MovieApi()),
        ),
        BlocProvider<SimilarmovieCubit>(
          create: (_) => SimilarmovieCubit(SimilarApi()),
        ),
        BlocProvider<MoviedetailsCubit>(
          create: (_) => MoviedetailsCubit(MovieApi()),
        ),
        BlocProvider<RecommendationmovieCubit>(
          create: (_) => RecommendationmovieCubit(RecommendationsApi()),
        ),
        BlocProvider<CastCubit>(
          create: (_) => CastCubit(CastApi()),
        ),
        BlocProvider<ReviewCubit>(
          create: (_) => ReviewCubit(ReviewApi()),
        ),
        BlocProvider<TopratedCubit>(
            create: (context) => TopratedCubit(MovieApi())),
        BlocProvider<UpcomingCubit>(
            create: (context) => UpcomingCubit(MovieApi()))
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const Home(),
      ),
    );
  }
}
