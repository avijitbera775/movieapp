import 'dart:convert';
import 'package:movieapp/config/key.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/config/url.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Movie.dart';
import 'package:http/http.dart' as http;
import 'package:movieapp/model/MovieDetailModel.dart';
import 'package:movieapp/model/model.api/movieDetailMode.api.dart';
import 'package:movieapp/model/model.api/populerModel.Api.dart';

class MovieApi {
  Future<PopulerModelApi> getUpcomingMovieApi() async {
    try {
      var response =
          await http.get(Uri.parse("${url}/movie/upcoming?api_key=${apiKey}"));
      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        var vb = data['results'];
        List<Movie> movieList = [];

        for (var item in vb) {
          Movie movie = movieFromJson(item);
          movieList.add(movie);
        }
        return PopulerModelApi(
            success: const Success("success"),
            movieList: movieList,
            apiStatus: ApiStatus.SUCCESS);
      } else {
        return PopulerModelApi(
            failer: const Failer("error"), apiStatus: ApiStatus.FAILED);
      }
    } catch (error) {
      return PopulerModelApi(
          failer: const Failer("error"), apiStatus: ApiStatus.FAILED);
    }
  }

  Future<PopulerModelApi> getTopRatedMovies() async {
    try {
      var response =
          await http.get(Uri.parse("${url}/movie/top_rated?api_key=${apiKey}"));
      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        var vb = data['results'];
        List<Movie> movieList = [];

        for (var item in vb) {
          Movie movie = movieFromJson(item);
          movieList.add(movie);
        }
        return PopulerModelApi(
            success: const Success("success"),
            movieList: movieList,
            apiStatus: ApiStatus.SUCCESS);
      } else {
        return PopulerModelApi(
            failer: const Failer("error"), apiStatus: ApiStatus.FAILED);
      }
    } catch (error) {
      return PopulerModelApi(
          failer: const Failer("error"), apiStatus: ApiStatus.FAILED);
    }
  }

  Future<MovieDetailApiModel?> getMovieDetails({required int movieId}) async {
    try {
      var response = await http
          .get(Uri.parse("${url}/movie/${movieId}?api_key=${apiKey}"));
      if (response.statusCode == 200) {
        MovieDetailModel details = movieDetailModelFromJson(response.body);

        return MovieDetailApiModel(
            apiStatus: ApiStatus.SUCCESS,
            success: const Success("success"),
            movieDetails: details);
      } else {
        return MovieDetailApiModel(
            apiStatus: ApiStatus.FAILED, failer: const Failer("error"));
      }
    } catch (error) {
      return MovieDetailApiModel(
          apiStatus: ApiStatus.FAILED, failer: const Failer("error"));
    }
  }

  Future<PopulerModelApi> getPopulerMovies() async {
    try {
      var response =
          await http.get(Uri.parse("${url}/movie/popular?api_key=${apiKey}"));
      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        var vb = data['results'];
        List<Movie> movieList = [];

        for (var item in vb) {
          Movie movie = movieFromJson(item);
          movieList.add(movie);
        }
        return PopulerModelApi(
            success: const Success("success"),
            movieList: movieList,
            apiStatus: ApiStatus.SUCCESS);
      } else {
        return PopulerModelApi(
            failer: const Failer("error"), apiStatus: ApiStatus.FAILED);
      }
    } catch (error) {
      return PopulerModelApi(
          failer: const Failer("error"), apiStatus: ApiStatus.FAILED);
      ;
    }
  }
}
