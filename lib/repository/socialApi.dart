import 'package:http/http.dart' as http;
import 'package:movieapp/config/key.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/config/url.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/model/model.api/socialModel.Api.dart';

class SocialApi {
  Future<SocialApiModel?> getSocialLinks({required int movieId}) async {
    try {
      var response = await http.get(
          Uri.parse("${url}/movie/${movieId}/external_ids?api_key=${apiKey}"));
      if (response.statusCode == 200) {
      } else {}
    } catch (error) {
      return SocialApiModel(
          apiStatus: ApiStatus.FAILED, failer: Failer("error"));
    }
  }
}
