import 'dart:convert';

import 'package:movieapp/config/key.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/config/url.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Cast.dart';
import 'package:http/http.dart' as http;
import 'package:movieapp/model/model.api/castModel.Api.dart';
import 'package:movieapp/model/model.api/castModel.Api.dart';

class CastApi {
  Future<CastApiModel> getCastApi({required int movieId}) async {
    try {
      List<Cast> castList = [];
      var response = await http
          .get(Uri.parse("${url}/movie/${movieId}/credits?api_key=${apiKey}"));

      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        for (var item in data['cast']) {
          Cast cast = castFromJson(item);
          castList.add(cast);
        }
        return CastApiModel(
            failer: null,
            success: const Success("Success"),
            castList: castList,
            apiStatus: ApiStatus.SUCCESS);
      } else {
        return CastApiModel(
            failer: const Failer("Error"), apiStatus: ApiStatus.FAILED);
      }
    } catch (error) {
      return CastApiModel(
          failer: const Failer("Error"), apiStatus: ApiStatus.FAILED);
    }
  }
}
