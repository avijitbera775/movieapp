import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:movieapp/config/key.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/config/url.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Reviews.dart';
import 'package:movieapp/model/model.api/reviewModel.Api.dart';

class ReviewApi {
  Future<ReviewModelApi> getMovieReviewsApi({required int movieId}) async {
    try {
      var response = await http
          .get(Uri.parse("${url}/movie/${movieId}/reviews?api_key=${apiKey}"));

      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        List<Reviews> review = reviewsFromJson(data['results']);
        return ReviewModelApi(
            apiStatus: ApiStatus.SUCCESS,
            currentPage: data['page'],
            totalPage: data['total_pages'],
            reviews: review,
            success: Success("success"));
      } else {
        return ReviewModelApi(
            apiStatus: ApiStatus.FAILED, failer: Failer("error"));
      }
    } catch (error) {
      return ReviewModelApi(
          failer: const Failer("error"), apiStatus: ApiStatus.FAILED);
    }
  }
}
