import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:movieapp/config/key.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/config/url.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Movie.dart';
import 'package:movieapp/model/model.api/recommendationsModel.api.dart';

class RecommendationsApi {
  Future<RecommendationModelApi> getRecommendationsApi(
      {required int movieId}) async {
    try {
      var response = await http.get(Uri.parse(
          "${url}/movie/${movieId}/recommendations?api_key=${apiKey}"));
      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        List<Movie> movieList = [];
        for (var item in data['results']) {
          Movie movie = movieFromJson(item);
          movieList.add(movie);
        }
        return RecommendationModelApi(
            recommendationList: movieList,
            currentPage: data['page'],
            totalPage: data['total_pages'],
            success: const Success("Success"),
            apiStatus: ApiStatus.SUCCESS);
      } else {
        return RecommendationModelApi(
            failer: const Failer("error"), apiStatus: ApiStatus.FAILED);
      }
    } catch (error) {
      return RecommendationModelApi(
          failer: const Failer("error"), apiStatus: ApiStatus.FAILED);
    }
  }
}
