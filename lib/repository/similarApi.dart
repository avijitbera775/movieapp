import 'package:http/http.dart' as http;
import 'package:movieapp/config/key.dart';
import 'package:movieapp/config/types.dart';
import 'package:movieapp/config/url.dart';
import 'package:movieapp/handaler/Failer.dart';
import 'package:movieapp/handaler/Success.dart';
import 'package:movieapp/model/Similar.dart';
import 'package:movieapp/model/model.api/similarModel.api.dart';

class SimilarApi {
  Future<SimilarModelApi> getSimilarApi({required int movieId}) async {
    try {
      var response = await http
          .get(Uri.parse("${url}/movie/${movieId}/similar?api_key=${apiKey}"));
      if (response.statusCode == 200) {
        Similar similar = similarFromJson(response.body);
        return SimilarModelApi(
            success: const Success("success"),
            similar: similar.results,
            apiStatus: ApiStatus.SUCCESS,
            currentPage: similar.page,
            totalPage: similar.totalPages);
      } else {
        return SimilarModelApi(
            failer: const Failer("Error"), apiStatus: ApiStatus.FAILED);
      }
    } catch (error) {
      return SimilarModelApi(
          failer: const Failer("Error"), apiStatus: ApiStatus.FAILED);
    }
  }
}
