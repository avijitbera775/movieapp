# MovieApp

- This project created with flutter
<br>
- Cuibt for application state management
- I built this app using [themoviedb](https://api.themoviedb.org/3) api

#
## Screenshots


![image](screenshots/Screenshot_1.jpg)
![image](screenshots/Screenshot_2.jpg)
![image](screenshots/Screenshot_3.jpg)

#
## Project Setup
- Clone or Download this project
- Run 'pub get' or 'flutter pub get' in terminal
- Change api key and token inside 'lib/config/key.dart' file
- Run 'flutter run' in terminal
<br>


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
